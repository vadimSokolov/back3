<?php

header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  if (!empty($_GET['save'])) {
    echo 'Спасибо, результаты сохранены.';
  }
  include('index.html');
  exit();
}

$name = $_POST['name'];
$email = $_POST['email'];
$birthYear = $_POST['birthYear'];
$sex = $_POST['sex'];
$countlimbs = $_POST['countlimbs'];
$superpower = $_POST['superpower'];
$biography = $_POST['biography'];
$check1 = $_POST['check1'];


$errors = FALSE;
if (empty($_POST['name'])) {
  echo "<script type='text/javascript'>alert('Заполните имя.');</script>";
  $errors = TRUE;
  exit();
}


if ($email == '') {
  echo "<script type='text/javascript'>alert('Заполните e-mail.');</script>";
  $errors = TRUE;
  exit();
}


if (empty($_POST['birthYear'])) {
  echo "<script type='text/javascript'>alert('Заполните дату рождения.');</script>";
  $errors = TRUE;
  exit();
}


if (empty($_POST['sex'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
  exit();
}


if (empty($_POST['countlimbs'])) {
  echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
  $errors = TRUE;
  exit();
}

if (empty($_POST['superpower'])) {
	echo "<script type='text/javascript'>alert('Заполните данные.');</script>";
	$errors = TRUE;
	exit();
}

if (empty($_POST['biography'])) {
  echo("<script type='text/javascript'>alert('Заполните биографию.');</script>");
  $errors = TRUE;
  exit();
}


if (empty($_POST['check1'])) {
  echo("<script type='text/javascript'>alert('Поставьте соглашение.');</script>");
  $errors = TRUE;
  exit();
}

if ($errors) {
  exit();
}

$user = 'u24098';
$pass = '23463546';
$db = new PDO('mysql:host=localhost;dbname=u24098', $user, $pass);

try {
  $stmt = $db->prepare(
	  "INSERT INTO form (name,email,birthYear,sex,countlimbs,superpower,biography,check1) 
  		VALUE (:name,:email,:birthYear,:sex,:countlimbs,:superpower,:biography,:check1)");
  $stmt -> execute(['name'=>$name,'email'=>$email,'birthYear'=>$birthYear,'sex'=>$sex,'countlimbs'=>$countlimbs,'superpower'=>$superpower,
  'biography'=>$biography,'check1'=>$check1]);
  echo "<script type='text/javascript'>alert('Спасибо, результаты сохранены.');</script>";
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
